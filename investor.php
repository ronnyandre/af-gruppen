<? require 'header.php' ?>

<div id="content">
	
	<section class="investor">
		<ul>
			<li>
				AFG <br>
				<strong>59,25</strong>
			</li>
			<li>
				Endring <br>
				<strong>1,25 <span class="positive">(2,16) <i class="icon-up-big"></i></span></strong>
			</li>
			<li class="right">
				Tid <br>
				<strong>15:30</strong>
			</li>
		</ul>
	</section>
	
	<section class="stock-messages news-list">
		
		<h2>Børsmeldinger</h2>
		
		<ul>
			<li>
				<a href="#">
					<span class="date">09.09.2013</span>
					<h4>Purchase of own shares</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">09.09.2013</span>
					<h4>Kjøp av egne aksjer</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">09.09.2013</span>
					<h4>Finansiell kalender 2014</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">09.09.2013</span>
					<h4>Finansiell kalender 2014</h4>
				</a>
			</li>
		</ul>
		
	</section>
	
	<section class="reports news-list">
		
		<h2>Rapporter</h2>
		
		<ul>
			<li>
				<a href="#">
					<h4>Årsrapport 2012 <span>(PDF)</span></h4>
				</a>
			</li>
			<li>
				<a href="#">
					<h4>Årsrapport 2011 <span>(PDF)</span></h4>
				</a>
			</li>
			<li>
				<a href="#">
					<h4>Kvartalsrapport 2. kvartal 2013 <span>(PDF)</span></h4>
				</a>
			</li>
			<li>
				<a href="#">
					<h4>Kvartalsrapport 1. kvartal 2013 <span>(PDF)</span></h4>
				</a>
			</li>
			<li>
				<a href="#">
					<h4>Kvartalspresentasjon 2. kvartal 2013 <span>(PDF)</span></h4>
				</a>
			</li>
			<li>
				<a href="#">
					<h4>Kvartalspresentasjon 1. kvartal 2013 <span>(PDF)</span></h4>
				</a>
			</li>
		</ul>
		
	</section>
	
	<section class="newsletter">
		
		<h2>Registrer deg for webcast 3. kvartal 2013</h2>
		
		<p>AF Gruppens kvartalspresentasjoner sendes direkte via webcast fra Hotel Continental i Oslo. Skriv inn din mailadresse dersom du ønsker å motta en melding i forkant av neste kvartalspresentasjon.</p>
		
		<form action="#">
			<input type="email" placeholder="Din e-postadresse">
			<button type="submit">Send inn</button>
		</form>
		
	</section>
	
</div><!-- #content -->

<? require 'footer.php' ?>