AF = {
	common: {
		init: function() {
			
			// TODO: Remove this in production
			$('a[href=#]').on('click tap', function(e) {
				e.preventDefault();
			});
			
			// initiate fastclick
			AF.common.fastclick();
			
			// set up main menu
			AF.common.mainmenu();
			
			// scroll past navigationbar
			window.scrollTo(0, 1);
			
			// initiate fitvids
			$('.reference-project').fitVids();
			
			// search box
			AF.common.search();
			
			// scroll to top
			AF.common.scrollTop();
			
			// filtering projects
			AF.common.filterProjects();
		},
		
		fastclick: function() {
			FastClick.attach(document.body);
		},
		
		scrollTop: function() {
			
			var $scrollBtn = $('.scroll-top a');
			
			$scrollBtn.on('click tap', function(e) {
				e.preventDefault();
				$('html, body').animate({ scrollTop: 0 });
			});
			
		},
		
		filterProjects: function() {
			
			var $selector = $('#projects-selector');
			
			$selector.on('change', function(e) {
				e.preventDefault();
				
				var category = $(this).find('option:selected').val();
				
				$('body').find('article[data-category!="' + category + '"]').fadeOut(100, function() {
					$('body').find('article[data-category="' + category + '"]').fadeIn();
				});
			});
						
		},
		
		search: function() {
			
			var $searchBox = $('#header .search'),
				$searchBtn = $('.toggle-search');
			
			// click event on search button
			$searchBtn.on('click tap', function(e) {
				e.preventDefault();
				$(document).trigger('search:toggle');
			});
			
			// hide search box
			$(document).on('search:hide', function(e) {
				e.preventDefault();
				
				// remove active class
				$searchBtn.removeClass('active');
				
				// hide search box
				$searchBox.slideUp(100);
			});
			
			// show search box
			$(document).on('search:show', function(e) {
				e.preventDefault();
				
				// add active class
				$searchBtn.addClass('active');
				
				// show search box
				$searchBox.slideDown(100);
			});
			
			// toggle search box
			$(document).on('search:toggle', function(e) {
				e.preventDefault();
				
				// hide menu
				$(document).trigger('menu:hide');
				
				if ($searchBox.is(':visible'))
					$(document).trigger('search:hide');
				else
					$(document).trigger('search:show');
			});
			
		},
		
		mainmenu: function() {
			
			var $menuToggler = $('.toggle-menu'),
				$menuOverlay = $('.overlay');
			
			var $menu = $menuToggler.next('ul');
			
			// click event on menu button
			$menuToggler.on('click tap', function(e) {
				e.preventDefault();
				$(document).trigger('menu:toggle');
			});
			
			// click event on menu overlay
			$menuOverlay.on('click tap', function(e) {
				e.preventDefault();
				$(document).trigger('menu:hide');
			});
			
			// hide menu
			$(document).on('menu:hide', function(e) {
				e.preventDefault();
				
				// hide menu
				$menu.slideUp(100);
				
				// remove active class
				$menuToggler.removeClass('active');
				
				// hide menu overlay
				$menuOverlay.fadeOut(100);
			});
			
			$(document).on('menu:show', function(e) {
				e.preventDefault();
				
				// show menu
				$menu.slideDown(100);
				
				// add active class
				$menuToggler.addClass('active');
				
				// hsow menu overlay
				$menuOverlay.fadeIn(100);
			});
			
			// toggle menu
			$(document).on('menu:toggle', function(e) {
				e.preventDefault();
				
				// hide search box
				$(document).trigger('search:hide');
				
				if ($menu.is(':visible'))
					$(document).trigger('menu:hide');
				else
					$(document).trigger('menu:show');
			});
		}
	}
};

UTIL = {
	exec: function(controller, action) {
		var ns = AF,
			action = (action === undefined) ? 'init' : action;
		
		if (controller !== '' && ns[controller] && typeof ns[controller][action] == 'function')
			ns[controller][action]();
	},
	init: function() {
		var body = document.body,
			controller = body.getAttribute('data-controller'),
			action = body.getAttribute('data-action');
		
		UTIL.exec('common');
		UTIL.exec(controller);
		UTIL.exec(controller, action);
	}
};

$(document).ready(UTIL.init);