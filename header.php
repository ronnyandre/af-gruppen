<!DOCTYPE html>
<html lang="no" class="mobile no-js">
	<head>
		<meta charset="utf-8" />
		<title>AF Gruppen</title>
		<meta name="description" content="" />
		<meta name="HandheldFriendly" content="True" />
		<meta name="MobileOptimized" content="320" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta http-equiv="cleartype" content="on" />
		
		<!-- Apple touch icons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/logo/144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/logo/114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/logo/72x72.png" />
		<link rel="apple-touch-icon-precomposed" href="images/logo/57x57.png" />
		<link rel="shortcut icon" href="images/logo/16x16.png" />
		
		<!-- Tile icons for Win 8 -->
		<meta name="msapplication-TileImage" content="/images/logo/144x144.png" />
		<meta name="msapplication-TileColor" content="#ffd600" />
		
		<link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<!--[if IE]>
			<link href="stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<![endif]-->
		<link href="stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
		
		<!-- Modernizr -->
		<script src="bower_components/modernizr/modernizr.js"></script>
	</head>
	<body>
		
		<div class="overlay"></div>
		
		<div id="wrapper">
			
			<div id="header">
				<nav>
					<ul class="main-nav">
						<li>
							<a href="#" class="toggle-menu">
								<i class="icon-menu"></i>
							</a>
							<ul style="display: none">
								<li><a href="#">Kontakt</a></li>
								<li><a href="#">Om AF Gruppen</a></li>
								<li><a href="#">Ledige stillinger</a></li>
								<li><a href="#">Hvordan er det å jobbe i AF</a></li>
								<li><a href="#">Referanseprosjekter</a></li>
								<li><a href="#">Anlegg</a></li>
								<li><a href="#">Bygg</a></li>
								<li><a href="#">Eiendom</a></li>
								<li><a href="#">Miljø</a></li>
								<li><a href="#">Energi</a></li>
								<li><a href="#">Offshore</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="toggle-search">
								<i class="icon-search"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-lock"></i>
							</a>
						</li>
					</ul>
				</nav>
				<a href="/" id="logo">AF Gruppen</a>
				
				<div class="search" style="display: none;">
					<form action="#">
						<input type="text" name="search" placeholder="Søk her" />
						<button type="submit">Søk</button>
					</form>
				</div>
			</div><!-- #header -->