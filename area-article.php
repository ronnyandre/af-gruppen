<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h2><a href="#">Bygg</a></h2>
		<h1>Næringsbygg og offentlig bygg</h1>
		
		<img src="http://lorempixel.com/320/198" alt="" class="article-image">
		
		<div class="article-intro">
			<p>AF har solid kompetanse og erfaring med oppføring av næringsbygg og offentlige bygg. Vi tilbyr fleksible løsninger av høy kvalitet ut fra kundens behov, ønsker og muligheter.</p>
		</div>
		
		<p>Vårt brede fagmiljø innen bygg og energi er vant til å jobbe tverrfaglig innenfor bygg, miljø og energioptimalisering av næringsbygg og institusjonsbygg.</p>
		
		<p>Bygg som stiller store krav til tekniske og funksjonelle løsninger, er utfordringer vi liker. Vi er kjent for å skape gode lokaler i forhold til kundens behov og krav. Enten bygget er stort eller lite, følger vi ditt prosjekt i mål på en kostnadseffektiv måte.</p>
		
		<p>AF oppnår meget gode innkjøpsavtaler.  Vi utfører arbeidet med egne håndverkere innenfor flere fagområder. I tillegg har vi knyttet til oss dyktige underentreprenører og fagfolk.</p>
			
	</article>
	
	<section class="share-links">
		
		<span>Del siden på:</span>
		
		<ul class="social-links right">
			<li>
				<a href="#" class="rounded share-tw">
					<i class="icon-twitter"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-li">
					<i class="icon-linkedin"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-gp">
					<i class="icon-gplus"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-fb">
					<i class="icon-facebook"></i>
				</a>
			</li>
			<li>
				<a href="#" class="share-em">
					<i class="icon-mail"></i>
				</a>
			</li>
		</ul>
		
	</section><!-- .share-links -->
	
	<section class="news-list project-listing">
		
		<h2>Referanseprosjekter</h2>
		
		<ul>
			<li>
				<a href="#">
					<img src="http://lorempixel.com/400/265" alt="">
					<h3>Konserthuset Kilden</h3>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://lorempixel.com/400/265" alt="">
					<h3>Halden Fengsel</h3>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="http://lorempixel.com/400/265" alt="">
					<h3>Park Inn Gardermoen</h3>
				</a>
			</li>
		</ul>
		
	</section><!-- .news-list -->
	
</div><!-- #content -->

<? require 'footer.php' ?>