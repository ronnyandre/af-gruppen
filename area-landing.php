<? require 'header.php' ?>

<div id="content">
	
	<section class="content-slider">
		
		<div class="slides">
			
			<div>
				<img src="http://lorempixel.com/400/265/sports" alt="" />
				<h2>Overskrift til banner her</h2>
			</div>
			
		</div>
		
	</section>
	
	<section class="business-areas" style="margin-top: -6px">
		
		<nav>
			<ul>
				<li><a href="#">Samferdsel</a></li>
				<li><a href="#">Tunnel og underjord</a></li>
				<li><a href="#">Vannkraft</a></li>
				<li><a href="#">Havneanlegg</a></li>
				<li><a href="#">Olje- og gassanlegg</a></li>
			</ul>
		</nav>
		
	</section><!-- .business -->
	
	<section class="introduction">
		<p>AF har erfaring og kompetanse til å gjennomføre alt fra mindre til store og krevende anleggsprosjekter, samt anleggsrelaterte nisjeprosjekter Våre kunder er hovedsakelig offentlige og kommunale etater, samt større industriselskaper. Anleggsvirksomheten utføres i Norge og i Stockholms-regionen.</p>
	</section><!-- .introduction -->
	
	<section class="projects">
		
		<nav>
			<ul>
				<li><a href="#">Referanseprosjekter</a></li>
			</ul>
		</nav>
		
		<div class="reference-project">
			<iframe width="640" height="360" src="//www.youtube.com/embed/5iSSXFxsjVM" frameborder="0" allowfullscreen></iframe>
		</div>
		
	</section><!-- .projects -->
	
</div><!-- #content -->

<? require 'footer.php' ?>