<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h1>Stikningsledere</h1>
		
		<div class="article-intro">
			<p>AF Anlegg søker engasjerte og erfarne stikningsledere.</p>
		</div>
		
		<p>Vi søker deg som trives i miljøer der krav til kompetanse, HMS, disiplin og ryddighet er stort. Den vi søker må beherske rollen som både fagperson og leder. Du må ha gode samarbeidsevner, initiativrik, stor arbeidskapasitet og fleksibel i forhold til arbeidssted. Kjenner du deg igjen, oppfordres du til å søke.</p>
		
		<p><strong>Arbeidsoppgaver:</strong></p>
		
		<ul>
			<li>Mottak og kontroll av arbeidsgrunnlag</li>
			<li>Bistand og tilrettelegging i fm. oppmålingsarbeid</li>
			<li>Opplæring og oppfølging av stikningsingeniører</li>
			<li>Sikre og utarbeide underlag for målebrev</li>
			<li>Utarbeidelse av dokumentasjon til FKB/NVDB</li>
			<li>Mengdeberegninger</li>
		</ul>
		
		<p><strong>Kvalifikasjoner:</strong></p>
		
		<ul>
			<li>Lenger erfaring som stikningsingeniør</li>
			<li>Erfaring fra bruk av Bever Control og Atlas boresystemer</li>
			<li>Gemini terreng superbruker og lang erfaring innen mengdeberegning</li>
			<li>Grunnleggende god kunnskap på Leica landmålingsutstyr</li>
		</ul>
		
		<p><strong>Vi tilbyr:</strong></p>
		
		<ul>
			<li>Trivelig arbeidsmiljø og utfordrende arbeidsoppgaver</li>
			<li>Konkurransedyktig lønn med attraktiv bonusavtale</li>
			<li>Aksjeprogram i AF Gruppen</li>
			<li>Gode utviklings- og karrieremuligheter i et konsern som er i vekst</li>
			<li>Godt faglig miljø</li>
		</ul>
		
		<p>
			<strong>Arbeidssted:</strong><br>
			Oslo <br>
			<strong>Søknadsfrist:</strong><br>
			01.08.2013
		</p>
		
		<h2 class="subtitle">Kontaktpersoner</h2>
		
		<ul class="contact">
			<li class="yellow">
				<a href="tel://004722222222">
					<i class="icon-phone phone right"></i>
					Morten Midtskog, Direktør <br>
					mob: +47 900 84 911
				</a>
			</li>
			<li class="yellow">
				<a href="tel://004722222222">
					<i class="icon-phone phone right"></i>
					Sif Løvdal <br>
					mob: +47 457 86 585
				</a>
			</li>
		</ul>
		
		<div class="gray-box">
			<p>
				<strong>Slik søker du på stillingen:</strong><br>
				AF Gruppen har valgt et online rekrutteringsverktøy som bidrar til å profesjonalisere våre rekrutteringsprosesser. Når du har lagt inn din CV i vår database, får du tilsendt en mail med beskjed at søknaden din er registret. På den måten sikrer vi en rettferdig behandling av alle søknadene.
			</p>
			<p>Vi anbefaler deg å søke på stillingen fra din PC/mac.</p>
		</div>
		
		<ul class="contact">
			<li class="yellow">
				<a href="#">
					<i class="icon-mail mail right"></i>
					Klikk her for å få informasjon om stillingen på e-post
				</a>
			</li>
		</ul>
		
		<h1>Om AF anlegg</h1>

		<p>AF Anlegg gjennomfører alle typer anleggsprosjekter innenfor samferdsel, infrastruktur og vannkraft, inkludert veg, bane, flyplass, tunnel, tunnelinnredning og sikring. Anleggsvirksomheten omfatter også nybygging og oppgradering av olje- og gassanlegg, samt havneanlegg. I tillegg har AF ressurser til å ivareta alle disipliner i EPCIC offshore prosjekter.</p>
		
		<p>Vi er en sentral aktør på de største prosjektene innen samferdsel, vannkraft, olje og gass som utføres i Norge.</p>
		
	</article>
	
	<section class="share-links">
		
		<span>Del siden på:</span>
		
		<ul class="social-links right">
			<li>
				<a href="#" class="rounded share-tw">
					<i class="icon-twitter"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-li">
					<i class="icon-linkedin"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-gp">
					<i class="icon-gplus"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-fb">
					<i class="icon-facebook"></i>
				</a>
			</li>
			<li>
				<a href="#" class="share-em">
					<i class="icon-mail"></i>
				</a>
			</li>
		</ul>
		
	</section><!-- .share-links -->
	
	<section class="news-list">
		
		<h2>Aktuelt</h2>
		
		<ul>
			<li>
				<a href="#">
					<span class="date">16.08.2013</span>
					<h4>Presentasjon av resultat for 2. kvartal</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>AF signerer kontrakt på Rv 13 Ryfast, E03 Solbakktunnelen</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>Avtale om kjøp av Sandakerveien 100 m.fl.</h4>
				</a>
			</li>
		</ul>
		
	</section><!-- .news-list -->
	
</div><!-- #content -->

<? require 'footer.php' ?>