<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h1 class="black-title">Logg inn oversikt</h1>
		
		<h2 class="subtitle">Energioppfølgingssystem</h2>
		
		<p>Som kunde av AF Energi &amp; Miljøteknikk kan du her logge deg på for å se ditt energiforbruk og hvilken effekt dine enøktiltak gir. Er du ikke kunde allerede, prøv vår demoversjon.</p>
		
		<p>
			<strong>For våre kunder:</strong><br>
			<a href="#">logg inn eos</a>
		</p>
		
		<p>
			<strong>Demoversjon:</strong><br>
			<a href="#">logg inn eos demo</a>
		</p>
		
		<p><strong>Login:</strong> eos</p>
		
		<h2 class="subtitle">AF Gruppens medarbeidere</h2>
		
		<p>Her er link til AF Gruppens hjemmekontorløsning, webmail m.m.</p>
		<p>
			<strong>Citrix:</strong> <br>
			<a href="#">https://citrix.afgruppen.no</a>
		</p>
		<p>
			<strong>Webmail:</strong> <br>
			<a href="#">https://mail.afgruppen.no</a>
		</p>
		<p>
			<strong>Collaboration:</strong> <br>
			<a href="#">https://collab.afgruppen.no</a>
		</p>

		<p>
			Intranettet AF Tellus - pålogging fagarbeidere.
		</p>
		<p>
			<strong>Førstegangs pålogging / bytte av passord:</strong><br>
			<a href="#">https://home.microsoftonline.com/login.aspx</a>
		</p>
		<p>
			<strong>Senere pålogging:</strong><br>
			<a href="#">https://home.microsoftonline.com/login.aspx</a>
		</p>
		
		<h2 class="subtitle">Support og fjernstyring av PC</h2>
		
		<p>
			<strong>Support / servicedesk:</strong> <br>
			Ring tlf. <a href="tlf://004722222222">22 22 22 22</a>
		</p>
		
		<p>
			<strong>Fjernstyring:</strong> <br>
			Last ned <a href="#">TeamViewerQS</a> for PC
		</p>
		
	</article>
	
</div><!-- #content -->

<? require 'footer.php' ?>