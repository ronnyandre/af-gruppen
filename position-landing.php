<? require 'header.php' ?>

<div id="content">
	
	<article>
		<h1>Ledige stillinger</h1>
	</article>
	
	<section class="news-list position-listing">
		
		<!-- TODO: kanskje filtrering inn her -->
		
		<ul>
			<li>
				<a href="#">
					<span class="date">Miljø</span>
					<h4>Prosjektleder</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Miljø</span>
					<h4>HVAC / CVS ingeniør</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg</span>
					<h4>Stikningsleder</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg</span>
					<h4>Stikningsingeniører</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Energi</span>
					<h4>Prosjektleder</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Eiendom</span>
					<h4>Maskinførere</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Offshore</span>
					<h4>Ingeniør - Maskin</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg</span>
					<h4>Fagarbeider betong og grunnarbeid</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg</span>
					<h4>Ingeniører til Nyhamna Exp. Proj.</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg / Miljø / Bygg</span>
					<h4>Åpen søknad - Fagarbeider</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">Anlegg / Miljø / Bygg / Energi</span>
					<h4>Prosjektleder</h4>
				</a>
			</li>
		</ul>
		
	</section><!-- .news-list -->
	
</div><!-- #content -->

<? require 'footer.php' ?>