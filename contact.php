<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h1>Kontakt</h1>
		
		<p>
			<strong>AF Gruppen ASA</strong><br>
			AF Gruppen Norge AS
		</p>
		
		<p>
			<strong>Besøksadresse</strong><br>
			Innspurten 15, 0663 Oslo
		</p>
		
		<p>
			<img src="http://maps.googleapis.com/maps/api/staticmap?center=Innspurten+15,+0663+Oslo,+Norway&amp;zoom=17&amp;size=400x265&amp;maptype=roadmap&amp;markers=color:red%7C59.9171341,10.807869897652518&amp;sensor=false" alt="">
		</p>
		
		<p>
			<strong>Postadresse</strong><br>
			PB 6272 Etterstad, 0603 Oslo
		</p>
		
		<ul class="contact border">
			<li class="yellow">
				<a href="#">
					<i class="icon-mail mail right"></i>
					<strong>E-post</strong><br>
					firmapost@afgruppen.no
				</a>
			</li>
			<li class="yellow">
				<a href="#">
					<i class="icon-phone phone right"></i>
					<strong>Sentralbord</strong><br>
					+47 22 89 11 00
				</a>
			</li>
		</ul>
		
		<p><strong>Pressekontakt:</strong></p>
		
		<ul class="contact border">
			<li class="yellow">
				<a href="#">
					<i class="icon-phone phone right"></i>
					Wibecke Brusdal <br>
					+47 930 93 150
				</a>
			</li>
		</ul>
		
		<p><strong>HR-kontakt:</strong></p>
		
		<ul class="contact border">
			<li class="yellow">
				<a href="#">
					<i class="icon-phone phone right"></i>
					Navn Navnesen <br>
					+47 930 93 150
				</a>
			</li>
		</ul>
		
	</article>
	
</div><!-- #content -->

<? require 'footer.php' ?>