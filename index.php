<? require 'header.php' ?>

<div id="content">
	
	<section class="content-slider slider">
		
		<div class="slides">
			
			<div>
				<img src="http://lorempixel.com/400/247/sports" alt="" />
				<h2>Overskrift til banner her</h2>
			</div>
			
			<div>
				<img src="http://lorempixel.com/400/247/sports" alt="" />
				<h2>Overskrift til banner her</h2>
			</div>
			
			<div>
				<img src="http://lorempixel.com/400/247/sports" alt="" />
				<h2>Overskrift til banner her</h2>
			</div>
			
		</div>
		
	</section><!-- .slider -->
	
	<section class="business-area">
		<ul>
			<li>
				<a href="#">
					<img src="images/business-area-anlegg.svg" alt="" />
					Anlegg
				</a>
			</li>
			<li>
				<a href="#">
					<img src="images/business-area-bygg.svg" alt="" />
					Bygg
				</a>
			</li>
			<li>
				<a href="#">
					<img src="images/business-area-eiendom.svg" alt="" />
					Eiendom
				</a>
			</li>
			<li>
				<a href="#">
					<img src="images/business-area-miljo.svg" alt="" />
					Miljø
				</a>
			</li>
			<li>
				<a href="#">
					<img src="images/business-area-energi.svg" alt="" />
					Energi
				</a>
			</li>
			<li>
				<a href="#">
					<img src="images/business-area-offshore.svg" alt="" />
					Offshore
				</a>
			</li>
		</ul>
	</section><!-- .business-area -->
	
	<nav>
		<ul>
			<li><a href="#">Ledige stillinger</a></li>
			<li><a href="#">Hvordan er det å jobbe i AF</a></li>
		</ul>
	</nav>
	
	<section class="news-list">
		
		<h2>
			Aktuelt
			<a href="#" class="show-all">Vis alle</a>
		</h2>
		
		<ul>
			<li>
				<a href="#">
					<span class="date">16.08.2013</span>
					<h4>Presentasjon av resultat for 2. kvartal</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>AF signerer kontrakt på Rv 13 Ryfast, E03 Solbakktunnelen</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>Avtale om kjøp av Sandakerveien 100 m.fl.</h4>
				</a>
			</li>
		</ul>
		
	</section><!-- .news-list -->
	
	<section class="investor">
		<ul>
			<li>
				AFG <br>
				<strong>59,25</strong>
			</li>
			<li>
				Endring <br>
				<strong>1,25 <span class="positive">(2,16) <i class="icon-up-big"></i></span></strong>
			</li>
			<li class="right link">
				<a href="#">Investorinfo</a>
			</li>
		</ul>
	</section>
	
</div><!-- #content -->

<? require 'footer.php' ?>