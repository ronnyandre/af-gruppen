<? require 'header.php' ?>

<div id="content">
	
	<div class="project-header">
		
		<h1>Referanseprosjekter</h1>
		
		<select id="projects-selector">
			<option value="anlegg">Anlegg</option>
			<option value="bygg">Bygg</option>
			<option value="eiendom">Eiendom</option>
			<option value="miljo">Miljø</option>
			<option value="energi">Energi</option>
			<option value="offshore">Offshore</option>
		</select>
		
	</div>
	
	<article class="project" data-category="bygg">
		<img src="http://lorempixel.com/320/198" alt="" class="article-image">
		<h3>Bygg</h3>
		<h2>Konserthuset Kilden</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, explicabo laboriosam dolorem. Illo, nulla, ad, iusto, placeat sint veniam distinctio maxime illum vero esse dolor repudiandae neque fuga sed totam.</p>
	</article>
	
	<article class="project" data-category="anlegg">
		<img src="http://lorempixel.com/320/198" alt="" class="article-image">
		<h3>Anlegg</h3>
		<h2>Konserthuset Kilden</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, explicabo laboriosam dolorem. Illo, nulla, ad, iusto, placeat sint veniam distinctio maxime illum vero esse dolor repudiandae neque fuga sed totam.</p>
	</article>
	
	<article class="project" data-category="eiendom">
		<img src="http://lorempixel.com/320/198" alt="" class="article-image">
		<h3>Eiendom</h3>
		<h2>Konserthuset Kilden</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, explicabo laboriosam dolorem. Illo, nulla, ad, iusto, placeat sint veniam distinctio maxime illum vero esse dolor repudiandae neque fuga sed totam.</p>
	</article>
	
</div><!-- #content -->

<? require 'footer.php' ?>