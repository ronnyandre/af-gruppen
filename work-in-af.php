<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h1>Hvordan er det å jobbe i AF?</h1>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis id architecto tempore impedit quod consequatur rerum cum quo quisquam fugiat!</p>
		
		<p><img src="http://lorempixel.com/400/265" alt=""></p>
		
		<p><a href="#" class="article-link">- Arbeidsdagen varierer veldig</a></p>
		
		<p>Alice (32) jobber som Offshore Manager i AF Decom Offshore. Hun er sivilingeniør i kjemi (prosessteknologi) fra NTNU.</p>
		
	</article>
	
	<hr>
	
	<article>
		
		<p><img src="http://lorempixel.com/400/265" alt=""></p>
		
		<p><a href="#" class="article-link">En bærer av AF-kulturen</a></p>
		
		<p>En bærer av AF-kulturen Hans Olav (36) er direktør for AF Bygg Sør. Han er sivilingeniør maskinteknikk fra NTNU, med fordypning i produksjons- og kvalitetsteknikk og program for industriell økologi.</p>
		
	</article>
	
	<hr>
	
	<article>
		
		<p><img src="http://lorempixel.com/400/265" alt=""></p>
		
		<p><a href="#" class="article-link">- Det dukker alltid opp noe usett</a></p>
		
		<p>Hilde (28) er driftsleder i AF Anlegg. Hun er sivilingeniør med en mastergrad i byggeteknikk og arkitektur fra Universitetet for miljø og biovitenskap (UMB).</p>
		
	</article>
	
</div><!-- #content -->

<? require 'footer.php' ?>