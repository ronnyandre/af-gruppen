$(document).ready(function () {

    // tabs
    if ($(".tabs").length > 0) {
        $(".tabs a.video").each(function () {
            $(this).append("<img class='playTrigger' src='/images/videoTriggerStandalone.png' alt='Se film' />");
        });

        var count = 0;
        var numberSteps = $("#mainareadiv").find("input.hidden").val();
        $(".tabs").prepend("<div id='jqTabTriggers'><table><tr></tr></table></div>").addClass("jqTabs");
        $(".tabs .mediaBlock").each(function () {
            count++;
            if (numberSteps == "True")
                $("#jqTabTriggers tr").append("<td><div><span class='counter'>" + count + "</span> <span>" + $(this).find("input.main").val() + "</span></div></td>");
            else
                $("#jqTabTriggers tr").append("<td><div><span class='noCounter'>" + $(this).find("input.main").val() + "</span></div></td>");

            //append triggers next
            if ($(this).next().find("h2.main").text() != "") {
                $(this).append("<div class='nextTab'><span>" + $(this).next().find("h2.main").text() + "<span></span></span></div>");
            } else {
                $(this).append("<div class='nextTab'><span>&nbsp;</span></div>");
            }
        });
        if (count < 4) {
            $("#jqTabTriggers table").css("width", "80%");
        }
        //hide all but first tab contents
        $(".tabs .mediaBlock:gt(0)").addClass("hiddenTab");
        $("#jqTabTriggers td:eq(0)").addClass("selected");

        $("#jqTabTriggers td").live("click", function () {
            //reset selected (and sidekick)
            $("#jqTabTriggers td").removeClass("selected").removeClass("sidekick");
            $(this).addClass("selected");
            $(this).prev().addClass("sidekick");
            //hide tab contents
            $(".tabs .mediaBlock").addClass("hiddenTab");
            //show tab contents for current tab clicked
            $(".tabs .mediaBlock:eq(" + $(this).index() + ")").removeClass("hiddenTab");
        });
        //step through the tabs
        $(".nextTab span span").live("click", function () {
            //reset selected (and sidekick)
            $("#jqTabTriggers .sidekick").removeClass("sidekick");
            $("#jqTabTriggers .selected").removeClass("selected").addClass("sidekick").next().addClass("selected");
            //show next tab contents
            $(".tabs .mediaBlock").addClass("hiddenTab");
            $(".tabs .mediaBlock:eq(" + $("#jqTabTriggers .selected").index() + ")").removeClass("hiddenTab");
        });
    }

    // styled dropdowns
    $('.secondary-content select, .selectionMaker select, select').selectmenu();

    equalHeightFeatureItems();

    //video in lightbox
    $(".video").click(function () {
        $.fancybox({
            'padding': 0,
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'showCloseButton': true,
            'title': this.title,
            'width': 680,
            'height': 495,
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': 'swf',
            'swf': {
                'wmode': 'transparent',
                'allowfullscreen': 'true'
            }
        });
        return false;
    });

}); //end all

$(window).load(function () {
    equalHeightFeatureItems();
});

function equalHeightFeatureItems() {
    $(".featureItems .item").removeAttr("style");
    var tallest = 0;
    $(".featureItems .item").each(function () {
        thisHeight = $(this).height();
        if (thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    $(".featureItems .item").height(tallest);
}