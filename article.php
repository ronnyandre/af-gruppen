<? require 'header.php' ?>

<div id="content">
	
	<article>
		
		<h2>Aktuelt</h2>
		<h1>Tittel på artikkel inn her</h1>
		<div class="date">08.09.2013</div>
		
		<img src="http://lorempixel.com/320/198" alt="" class="article-image">
		
		<div class="article-intro">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam incidunt fuga eum molestiae voluptate quasi consectetur error hic sunt alias.</p>
		</div>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, explicabo laboriosam dolorem. Illo, nulla, ad, iusto, placeat sint veniam distinctio maxime illum vero esse dolor repudiandae neque fuga sed totam.</p>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, ipsum.</p>
		
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, veritatis obcaecati architecto repellendus delectus itaque neque porro ipsam quibusdam expedita reiciendis aperiam sed et eveniet doloremque sint doloribus nobis fugit unde incidunt? Repellendus, blanditiis, soluta, fuga iure accusamus pariatur iste necessitatibus ipsam architecto ex assumenda.</p>
		
	</article>
	
	<section class="share-links">
		
		<span>Del siden på:</span>
		
		<ul class="social-links right">
			<li>
				<a href="#" class="rounded share-tw">
					<i class="icon-twitter"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-li">
					<i class="icon-linkedin"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-gp">
					<i class="icon-gplus"></i>
				</a>
			</li>
			<li>
				<a href="#" class="rounded share-fb">
					<i class="icon-facebook"></i>
				</a>
			</li>
			<li>
				<a href="#" class="share-em">
					<i class="icon-mail"></i>
				</a>
			</li>
		</ul>
		
	</section><!-- .share-links -->
	
	<section class="news-list">
		
		<h2>Aktuelt</h2>
		
		<ul>
			<li>
				<a href="#">
					<span class="date">16.08.2013</span>
					<h4>Presentasjon av resultat for 2. kvartal</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>AF signerer kontrakt på Rv 13 Ryfast, E03 Solbakktunnelen</h4>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="date">10.07.2013</span>
					<h4>Avtale om kjøp av Sandakerveien 100 m.fl.</h4>
				</a>
			</li>
		</ul>
		
	</section><!-- .news-list -->
	
</div><!-- #content -->

<? require 'footer.php' ?>