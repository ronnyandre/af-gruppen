			<div id="footer">
				
				<!--
					skal ikke vises på:
						- forsiden
				-->
				<div class="scroll-top">
					<a href="#" class="right">Til toppen</a>
				</div>
				
				<ul class="bar">
					<li class="left"><a href="#">Kontaktinfo</a></li>
					<li class="right">
						<a href="#">
							<i class="icon-mail"></i>
						</a>
					</li>
					<li class="right">
						<a href="#">
							<i class="icon-phone"></i>
						</a>
					</li>
				</ul>
				
				<div class="links">
					
					<ul class="languages">
						<li><a href="#">Svenska</a></li>
						<li><a href="#">English</a></li>
						<li class="right"><a href="#">til webutgaven</a></li>
					</ul>
					
					<div class="share-links">
						<ul class="social-links right">
							<li>
								<a href="#" class="rounded share-tw">
									<i class="icon-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#" class="rounded share-li">
									<i class="icon-linkedin"></i>
								</a>
							</li>
							<li>
								<a href="#" class="rounded share-fb">
									<i class="icon-facebook"></i>
								</a>
							</li>
						</ul>
					</div><!-- .share-links -->
					
				</div><!-- .links -->
				
			</div><!-- #footer -->
			
		</div><!-- #wrapper -->
		
		<script src="bower_components/jquery/jquery.js"></script>
		<script src="bower_components/fitvids/jquery.fitvids.js"></script>
		<script src="javascripts/plugins.js"></script>
		<script src="javascripts/main.js"></script>
		
		<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
		<script>
			(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
			function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
			e=o.createElement(i);r=o.getElementsByTagName(i)[0];
			e.src='//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
			ga('create','UA-XXXXX-X');ga('send','pageview');
		</script>
		
	</body>
</html>